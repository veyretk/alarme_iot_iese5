# name of the application
APPLICATION?=button

# The default board is ST B-L072Z-LRWAN1 LoRa discovery board
BOARD ?= lora-e5-dev

RIOTBASE ?= $(CURDIR)/../../RIOT

#
# Here we add the modules that are needed
#
USEMODULE += xtimer

#thread
USEMODULE += shell
USEMODULE += shell_commands
USEMODULE += ps

#
# The application needs LoRaWAN related modules and variables:
#
USEMODULE += auto_init_loramac
USEMODULE += lm75a
# The Semtech LoRa radio device (SX126X)
USEMODULE += sx126x_stm32wl

# The Semtech Loramac package
USEPKG += semtech-loramac

# Default region is Europe and default band is 868MHz
LORA_REGION ?= EU868

USEMODULE += semtech_loramac_rx

FEATURES_REQUIRED += periph_gpio_irq
FEATURES_REQUIRED += periph_gpio

# Cybersecurity
USEMODULE += crypto
#USEMODULE += crypto_aes
USEMODULE += cipher_modes
USEMODULE += fmt

# Some boards do not initialize LED0 by default
CFLAGS=-DAUTO_INIT_LED0

include Makefile.include





// Créateurs : Killian Veyret - Jeremy Slama - IESE5 CSC
/* ***************************** Librairies ********************************** */
#include <stdio.h>
#include <stdint.h>

#include "board.h"
#include "periph/gpio.h"
#include "periph_conf.h"
#include "xtimer.h"
#include "thread.h"

#include <string.h>
#include "board.h"
#include "net/loramac.h"     /* core loramac definitions */
#include "semtech_loramac.h" /* package API */
#include "lm75.h"
#include "lm75_params.h"

#include "fmt.h"
#include "crypto/ciphers.h"
#include "crypto/modes/ctr.h"
#include <inttypes.h>

/* ****************************** Définition des variables ********************************* */
#define BTN0_INT_FLANK  GPIO_FALLING
#define BTN1_INT_FLANK  GPIO_FALLING

static bool alarm_manu = 0, alarm_temp = 0;
static int cpt = 0;
static int temperature = 0;
static char message[60];

static char stack[THREAD_STACKSIZE_MAIN];
static char stack2[THREAD_STACKSIZE_MAIN];
static char stack3[THREAD_STACKSIZE_MAIN];


/* Declare globally the sensor device descriptor */
extern semtech_loramac_t loramac;  /* The loramac stack descriptor */

/* Device and application informations required for OTAA activation */
static const uint8_t deveui[LORAMAC_DEVEUI_LEN] = { 0x84, 0x54, 0x03, 0xe4, 0x94, 0xf2, 0xf6, 0xbb };
static const uint8_t appeui[LORAMAC_APPEUI_LEN] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
static const uint8_t appkey[LORAMAC_APPKEY_LEN] = { 0xbd, 0xfc, 0xe5, 0x8e, 0xca, 0xe8, 0xdc, 0x43, 0xb1, 0xa1, 0x0b, 0xfa, 0xd7, 0xe8, 0x46, 0x2c };

static lm75_t lm75;

/* Intermediate encryption/decryption buffers  */
#define BUF_SIZE (64U)
static uint8_t data[BUF_SIZE] = { 0 };
static char buf_str[BUF_SIZE * 2] = { 0 };
static uint8_t ctr_copy[16];

// Clé symmétrique 128 bits
static const uint8_t key[] = {
    0x23, 0xA0, 0x18, 0x53, 0xFA, 0xB3, 0x89, 0x23,
    0x65, 0x89, 0x2A, 0xBC, 0x43, 0x99, 0xCC, 0x00
};

// Table de permutation
static const uint8_t ctr[] = {
    0xf0, 0xf1, 0xf2, 0xf3, 0xf4, 0xf5, 0xf6, 0xf7,
    0xf8, 0xf9, 0xfa, 0xfb, 0xfc, 0xfd, 0xfe, 0xff
};

/* **************************** Fonctions utilisées *********************************** */
//Fonctions d'interruption des boutons
static void cb_bp0(void *arg)
{
    printf("Alarme manuelle enclenchée !\n");
    gpio_clear((gpio_t)arg); // clear met à 1 la LED
    alarm_manu = 1;
}

static void cb_bp1(void *arg)
{
    if(alarm_manu || alarm_temp)
    {
    	printf("Alarmes désactivées\n");
    	alarm_manu = 0;
    	alarm_temp = 0;
    }
    else 
    	printf("Pas d'alarme activée\n");
    gpio_set((gpio_t)arg); // set met à 0 la LED
}

// Fonction de cryptage des données
static int _encrypt_handler(char* msg)
{
    /* Clear intermediate data buffer */
    memset(data, 0, BUF_SIZE);

    /* Copy the nonce in memory */
    memcpy(ctr_copy, ctr, 16);

    /* Encrypt the message */
    cipher_t cipher;
    cipher_init(&cipher, CIPHER_AES_128, key, sizeof(key));
    size_t enc_len = cipher_encrypt_ctr(&cipher, ctr_copy, 0, (uint8_t *)msg, strlen(msg), data);

    /* Convert the byte array to a string of hex characters */
    size_t len = fmt_bytes_hex(buf_str, data, enc_len);
    buf_str[len] = 0;

    /* Print the result */
    printf("%s\n", buf_str);

    return 0;
}

// Fonction de décryptage
static int _decrypt_handler(char* msg)
{
    /* Clear intermediate data buffer */
    memset(data, 0, BUF_SIZE);

    /* Copy the nonce in memory */
    memcpy(ctr_copy, ctr, 16);

    /* Convert encrypt message from hex string to byte array */
    size_t len = fmt_hex_bytes(data, msg);

    /* Decrypt the message */
    cipher_t cipher;
    cipher_init(&cipher, CIPHER_AES_128, key, sizeof(key));
    cipher_decrypt_ctr(&cipher, ctr_copy, 0, data, len, (uint8_t *)buf_str);
    buf_str[len] = 0;

    /* Print the result */
    printf("%s\n", buf_str);

    return 0;
}

/* ***************************** Tâches utilisées ********************************** */
// Tâche de gestion de l'alarme
static void *alarm_thread_handler(void *arg1)
{
    while(1){
    	// Fait sonner le buzzer en cas d'alarme
	if((alarm_manu == 1) || (alarm_temp == 1))
	    {
	    	cpt = 0;
		while(((alarm_manu == 1) || (alarm_temp == 1)) && (cpt<50)){ //100ms
			gpio_set((gpio_t)arg1);
			xtimer_usleep(903); // 554Hz
			gpio_clear((gpio_t)arg1);
			xtimer_usleep(903); // 554Hz
			cpt++;
		}
		cpt=0;
		while(((alarm_manu == 1) || (alarm_temp == 1)) && (cpt<200)){ //400ms
			gpio_set((gpio_t)arg1);
			xtimer_usleep(1136); // 440Hz
			gpio_clear((gpio_t)arg1);
			xtimer_usleep(1136); // 440Hz
			cpt++;
		}

	    }
	    
	    // Eteint le buzzer s'il n'y a pas d'alarme
	    else
	    	gpio_clear((gpio_t)arg1);
	    
	    xtimer_msleep(50);
    }
    return NULL;
}

// Tâche de gestion de la LED
static void *led_thread_handler(void *arg)
{
    while(1){
        // Heartbeat
    	if((alarm_manu == 0) && (alarm_temp == 0)){
	    // La LED D5 s'allume sur état bas
		//printf("Set pin to LOW\n");
		gpio_clear((gpio_t)arg);

		xtimer_msleep(50); // délais

		//printf("Set pin to HIGH\n");
		gpio_set((gpio_t)arg);

		xtimer_sleep(2); //délais de 2 secondes
	}
	
	// Alerte
	else{
		//printf("Set pin to LOW\n");
		gpio_clear((gpio_t)arg);

		xtimer_msleep(250);

		//printf("Set pin to HIGH\n");
		gpio_set((gpio_t)arg);

		xtimer_msleep(250);
	}
    }
    
    return NULL;
    
}

// Tâche de mesure de la température et de déclenchement des alarmes
static void *temp_thread_handler(void *arg)
{	
   	while(1){
   		// Récupère la température du capteur, la met en forme et l'affiche
		if (lm75_get_temperature(&lm75, &temperature)!= LM75_SUCCESS)
			printf("Cannot read temperature!");
		sprintf(message, "T:%d.%dC",
                (temperature / 1000), (temperature % 1000));
                
                // Encodage et décodage de la température
       		printf("%s\nencode : ", message);
       		_encrypt_handler(message);
       		printf("decode : ");
       		_decrypt_handler(buf_str);
		
		// Gestion des seuils qui déclenchent l'alarme de température	
		if(((temperature/1000) > 24) || ((temperature/1000) < 21)){
			alarm_temp = 1;
			printf("Alarme de température enclenchée !\n");
		}				
		else {
			alarm_temp = 0;
			printf("Alarme de température désactivée\n");
		}
			
		xtimer_sleep(5); // Calcul de la température toutes les 5 secondes
        }
    	return arg;
}

/* ***************************** Fonction principale ********************************** */
int main(void)
{
    //LED cablée sur D5 (PB5) :
    gpio_t pin_led = GPIO_PIN(PORT_B, 5); 
    if (gpio_init(pin_led, GPIO_OUT)) {
    	printf("Error to initialize GPIO_PIN(%d %d)\n", PORT_B, 5);
    return -1;
    }
   
    // thread led
    thread_create(stack, sizeof(stack), THREAD_PRIORITY_MAIN - 1, THREAD_CREATE_WOUT_YIELD | THREAD_CREATE_STACKTEST, led_thread_handler, (void *)pin_led, "thread led"); 
    // arguments : stack pointer, status, priority, pid, callback, param, message
    
    
    // Connexion des boutons
    if (gpio_init_int(BTN0_PIN, BTN0_MODE, BTN0_INT_FLANK, cb_bp0, (void *)pin_led) < 0) {
        printf("[FAILED] init BTN0!");
        return 1;
    }
    if (gpio_init_int(BTN1_PIN, BTN1_MODE, BTN1_INT_FLANK, cb_bp1, (void *)pin_led) < 0) {
        printf("[FAILED] init BTN1!");
        return 1;
    }
    
    //Buzzer cablé sur D10 (PB10) :
    gpio_t pin_buzz = GPIO_PIN(PORT_B, 10);
    if (gpio_init(pin_buzz, GPIO_OUT)) {
    	printf("Error to initialize GPIO_PIN(%d %d)\n", PORT_B, 10);
    	return -1;
    }
    
    // thread alarme
    thread_create(stack2, sizeof(stack2), THREAD_PRIORITY_MAIN - 1, THREAD_CREATE_WOUT_YIELD | THREAD_CREATE_STACKTEST, alarm_thread_handler, (void *)pin_buzz, "thread buzzer");
    
    // thread température
    thread_create(stack3, sizeof(stack3), THREAD_PRIORITY_MAIN - 1, THREAD_CREATE_WOUT_YIELD | THREAD_CREATE_STACKTEST, temp_thread_handler, (void *)NULL, "thread température");
    
    
    // Configure LoRa
    /* configure the device parameters */
    if (lm75_init(&lm75, &lm75_params[0]) != LM75_SUCCESS) {
        printf("Sensor initialization failed");
        return 1;
    }
    semtech_loramac_set_deveui(&loramac, deveui);
    semtech_loramac_set_appeui(&loramac, appeui);
    semtech_loramac_set_appkey(&loramac, appkey);
    
    /* change datarate to DR5 (SF7/BW125kHz) */
    semtech_loramac_set_dr(&loramac, 5);
    
    /* start the OTAA join procedure */
    if (semtech_loramac_join(&loramac, LORAMAC_JOIN_OTAA) != SEMTECH_LORAMAC_JOIN_SUCCEEDED) {
        printf("Join procedure failed");
        return 1;
    }
    printf("Join procedure succeeded");
    
    while(1){
    // Envoie de la température et de l'état des alarmes en LoRa    
        sprintf(message, "T:%d.%dC\nAlarme manuelle : %d\nAlarme de temperature : %d",
                (temperature / 1000), (temperature % 1000), alarm_manu, alarm_temp);

        if (semtech_loramac_send(&loramac,
                                 (uint8_t *)message, strlen(message)) != SEMTECH_LORAMAC_TX_DONE) {
            printf("Cannot send message '%s'\n", message);
        }
        else {
            printf("Message '%s' sent\n", message);
        }
        xtimer_sleep(10); // Envoie toutes les 10 secondes
    }
    return 0;
}
